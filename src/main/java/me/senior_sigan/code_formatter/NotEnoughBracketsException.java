package me.senior_sigan.code_formatter;

/**
 * Created by Ilya on 28.06.2014.
 */
public class NotEnoughBracketsException extends FormatterException {
    public NotEnoughBracketsException(){
        super();
    }

    public NotEnoughBracketsException(String message){
        super(message);
    }
    public NotEnoughBracketsException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotEnoughBracketsException(Throwable cause) {
        super(cause);
    }
}
