package me.senior_sigan.code_formatter;

import org.apache.log4j.Logger;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by Ilya on 28.06.2014.
 */
public class FormatOptions {
    Logger log = Logger.getLogger(FormatOptions.class);

    private final String TAB = "\t";
    private final String SPACE = " ";
    private final int DEFAULT_INDENT_SIZE = 4;
    private final String CRLF = "\r\n";
    private final String LF = "\n";

    //Default values
    private String indent = SPACE;
    private int indentSize = DEFAULT_INDENT_SIZE;
    private String newLine = LF;

    /**
     * Default constructor. Set all params to default
     */
    public FormatOptions(){
        this.setDefaultParams();
    }

    /**
     * In properties file set useSpaces: [true, false], indentSize: [int], CRLF: [true, false]
     * @param pathToConfig path to properties file
     */
    public FormatOptions(String pathToConfig){
        try {
            Properties properties = new Properties();
            properties.load(new FileInputStream(pathToConfig));

            Boolean useSpaces = Boolean.valueOf(properties.getProperty("useSpaces"));
            Integer indentSize  = Integer.valueOf(properties.getProperty("indentSize"));
            Boolean useCRLF = Boolean.valueOf(properties.getProperty("CRLF"));

            if (useSpaces) {
                this.useSpaces();
            } else {
                this.useTabs();
            }
            if (indentSize > 0) {
                this.setIndentSize(indentSize);
            }
            if (useCRLF){
                this.useCRLF();
            } else {
                this.useLF();
            }
            log.info("Using custom settings for formatter");
        } catch (IOException e){
            this.setDefaultParams();
            log.error(e.getMessage());
            log.info("Using default settings");
        }
    }

    public void setDefaultParams(){
        this.useLF();
        this.useSpaces();
        this.setIndentSize(-1);
    }

    /**
     * Set new line symbol to '\n' - Use in unix-like OS
     */
    public void useLF(){
        this.newLine = LF;
    }

    /**
     * Set new line symbol to '\r\n' - Use in windows OS - honestly, DON'T USE
     */
    public void useCRLF(){
        this.newLine = CRLF;
    }

    /**
     * Set using spaces with proper spaces count in indent
     */
    public void useSpaces(){
        this.indent = SPACE;
    }

    /**
     * Set using tabs
     */
    public void useTabs(){
        this.indent = TAB;
    }

    /**
     * Set count of indents
     * @param indentSize Use -1 to set default value
     */
    public void setIndentSize(int indentSize){
        if (indentSize > 0) {
            this.indentSize = indentSize;
        } else {
            this.indentSize = DEFAULT_INDENT_SIZE;
        }
    }

    public String getIndent(){
        return this.indent;
    }

    public int getIndentSize(){
        return this.indentSize;
    }

    /**
     *
     * @param count
     * @return repeat count times indent
     */
    public String getIndent(int count){
        return repeat(this.indent, count * this.indentSize);
    }

    public String getNewLine(){
        return this.newLine;
    }

    /**
     * This is for repeat string many times, like in Ruby "asa"*count
     * @param str
     * @param count
     * @return Repeated string
     */
    private String repeat(final String str, final int count){
        if (count <= 0){
            return "";
        }
        String ret = str;
        for (int i = 1; i < count; i++){
            ret+= str;
        }
        return ret;
    }
}
