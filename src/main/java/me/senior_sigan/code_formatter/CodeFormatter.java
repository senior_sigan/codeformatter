package me.senior_sigan.code_formatter;

/**
 * Created by Ilya Siganov on 28.06.2014.
 */

import me.senior_sigan.code_formatter.streams.InStream;
import me.senior_sigan.code_formatter.streams.OutStream;
import me.senior_sigan.code_formatter.streams.StreamException;
import org.apache.log4j.Logger;

/**
 * CodeFormatter class provide easy and fast method to format you java code
 * @author Ilya Siganov
 * @version 1.0
 */
public class CodeFormatter {
    private Logger log = Logger.getLogger(CodeFormatter.class);
    /**
     * <p>One way code stream format function. Relay only on  '{,},;'</p>
     * @param stream Input codeStream. Modified into function!!
     * @param formattedCode Output codeStream. Modified into function.
     * @throws java.io.IOException  If can't read or write in stream
     */
    public void format(
            final InStream stream,
            final OutStream formattedCode,
            final FormatOptions options
    ) throws FormatterException, StreamException {
        char current;
        int level = 0;
        boolean newLine = false;
        int parenthesis = 0;

        try {
            while (!stream.isEnd()){
                current = stream.readSymbol();

                switch (current){
                    case '(':
                        parenthesis++;
                        formattedCode.writeSymbol(current);
                        continue;
                    case ')':
                        parenthesis--;
                        formattedCode.writeSymbol(current);
                        continue;
                    case ';':
                        if (parenthesis==0) {        //this is just for 'for(var;condition;func){}'
                            newLine = true;
                        }
                        formattedCode.writeSymbol(current);
                        continue;
                    case '{':
                        newLine = true;
                        ++level;
                        formattedCode.writeSymbol(' ');
                        formattedCode.writeSymbol(current);
                        continue;
                    case '}':
                        newLine = true;
                        --level;
                        if (level < 0) {
                            throw new IndexOutOfBoundsException();
                        }
                        writeString(options.getNewLine() + options.getIndent(level) + current, formattedCode);
                        continue;
                }
                if (newLine){
                    newLine = false;
                    writeString(options.getNewLine() + options.getIndent(level) + current, formattedCode);
                } else {
                    formattedCode.writeSymbol(current);
                }
            }
        } catch (StreamException e){
            log.fatal(e.getMessage());
            throw e;
        }

        if (level != 0) {
            log.error("Not enough brackets.");
            throw new NotEnoughBracketsException("Not enough brackets.");
        }
    }

    /**
     * <p>Helper method to write string into the stream.</p>
     * @param str Input string
     */
    private void writeString(String str, OutStream stream) throws StreamException {
        for (int i = 0; i < str.length(); i++) {
            stream.writeSymbol(str.charAt(i));
        }
    }
}
