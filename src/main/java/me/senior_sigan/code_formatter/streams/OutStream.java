package me.senior_sigan.code_formatter.streams;

/**
 * Created by Ilya on 28.06.2014.
 */
public interface OutStream {
    public void writeSymbol(char ch) throws StreamException;
    public void close() throws StreamException;
}
