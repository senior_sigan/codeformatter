package me.senior_sigan.code_formatter.streams;

import org.apache.log4j.Logger;

import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 * Created by Ilya on 28.06.2014.
 */
public class FileOutStream implements OutStream {
    private PrintWriter printWriter;
    private Logger log = Logger.getLogger(FileInStream.class);

    /**
     * Constructor
     * @param fileName
     * @throws StreamException
     */
    public FileOutStream(String fileName) throws StreamException {
        try {
            this.printWriter = new PrintWriter(fileName);
            log.info("Open file for writing.");
        } catch (FileNotFoundException e) {
            log.fatal(e.getMessage());
            throw new StreamException(e.getMessage(), e.getCause());
        }
    }

    /**
     * Put single char into file stream
     * @param ch
     */
    @Override
    public void writeSymbol(char ch) {
        this.printWriter.print(ch);
    }

    /**
     * Close file stream and save changes
     */
    @Override
    public void close() {
        this.printWriter.close();
    }
}
