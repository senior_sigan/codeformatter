package me.senior_sigan.code_formatter.streams;

/**
 * Created by Ilya on 28.06.2014.
 */
public class StreamException extends Exception {
    public StreamException(){
        super();
    }

    public StreamException(String message){
        super(message);
    }
    public StreamException(String message, Throwable cause) {
        super(message, cause);
    }

    public StreamException(Throwable cause) {
        super(cause);
    }
}
