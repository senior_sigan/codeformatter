package me.senior_sigan.code_formatter.streams;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by Ilya on 28.06.2014.
 */
public class FileInStream implements InStream {
    private BufferedReader  buffer;
    private char nextChar;
    private int currentSymbol;
    private boolean nextEOF = false;
    private Logger log = Logger.getLogger(FileInStream.class);

    /**
     * Constructor
     * @param fileName
     * @throws StreamException
     */
    public FileInStream(final String fileName) throws StreamException {
        buffer = null;

        try {
            buffer = new BufferedReader(new FileReader(fileName));
            log.info("Creating buffer and opening file.");
        } catch (FileNotFoundException e){
            log.fatal(e.getMessage());
            throw new StreamException(e.getMessage(), e.getCause());
        }

        // Read one char ahead to verify is it isEnd or not
        try {
            log.info("Start reading from buffer");
            currentSymbol = buffer.read();
            if (currentSymbol != -1){
                nextChar = (char)currentSymbol;
            } else {
                nextChar = 0;
                nextEOF = true;
            }
        } catch (IOException e) {
            nextEOF = true;
        }
    }

    /**
     * Get single char from file stream
     * @return char
     * @throws StreamException
     */
    @Override
    public char readSymbol() throws StreamException {
        char currentChar = this.nextChar;

        try {
            this.currentSymbol = this.buffer.read();
            if (-1 != this.currentSymbol) {
                this.nextChar = (char)this.currentSymbol;
            } else {
                this.nextChar = 0;
                this.nextEOF = true;
            }
        } catch (IOException e) {
            this.nextEOF = true;
            log.error(e.getMessage());
            throw new StreamException(e.getMessage(),e.getCause());
        }

        return currentChar;
    }

    /**
     * Soft closing the stream
     * @throws StreamException
     */
    @Override
    public void close() throws StreamException {
        try {
            buffer.close();
            log.info("Buffer closed.");
        } catch (IOException e) {
            log.error(e.getMessage());
            throw new StreamException(e.getMessage(),e.getCause());
        }
    }

    /**
     * EOF or notEof - this is the question
     * @return true if isEnd, false - otherwise
     */
    @Override
    public boolean isEnd() {
        return nextEOF;
    }
}
