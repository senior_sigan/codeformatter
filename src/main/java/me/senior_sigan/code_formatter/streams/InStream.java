package me.senior_sigan.code_formatter.streams;

/**
 * Created by Ilya on 28.06.2014.
 */
public interface InStream {
    public char readSymbol() throws StreamException;
    public void close() throws StreamException;
    public boolean isEnd();
}
