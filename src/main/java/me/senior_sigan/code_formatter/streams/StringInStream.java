package me.senior_sigan.code_formatter.streams;

import org.apache.log4j.Logger;

/**
 * Created by Ilya on 28.06.2014.
 */
public class StringInStream implements InStream {
    private String inString;
    private Integer currentPos;
    private Integer length;
    private Logger log = Logger.getLogger(StringInStream.class);

    /**
     * Constructor
     * @param str - input string
     */
    public StringInStream(final String str) throws StreamException {
        if (str == null) {
            log.fatal("Null string");
            throw new StreamException("Null string");
        }
        this.inString = str;
        this.currentPos = 0;
        this.length = this.inString.length();
    }

    /**
     * Get next char from string stream
     * @return next char
     * @throws StreamException
     */
    @Override
    public char readSymbol() throws StreamException {
        this.currentPos++;
        if (this.currentPos > this.length) {
            log.error("Index out of range while getting symbol from string stream.");
            throw new StreamException("index out of range");
        }
        return this.inString.charAt(this.currentPos - 1);  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void close() throws StreamException {
        //nothing to do
    }

    /**
     *
     * @return true if isEnd
     */
    @Override
    public boolean isEnd() {
        return this.currentPos >= this.length;
    }
}
