package me.senior_sigan.code_formatter.streams;

import java.io.IOException;
import java.io.StringWriter;

/**
 * Created by Ilya on 28.06.2014.
 */
public class StringOutStream implements OutStream {
    private StringWriter stream;

    public StringOutStream(){
        this.stream = new StringWriter();
    }

    /**
     * Paste one symbol to stream
     * @param ch to put in stream
     */
    @Override
    public void writeSymbol(final char ch) {
        this.stream.append(ch);
    }

    @Override
    public void close() throws StreamException {
        try {
            this.stream.close();
        } catch (IOException e) {
            throw new StreamException(e.getMessage(), e.getCause());
        }
    }

    /**
     * Show content of stream
     * @return content of stream
     */
    public String show(){
        return this.stream.toString();
    }
}
