package me.senior_sigan.code_formatter;

import me.senior_sigan.code_formatter.streams.*;
import org.apache.log4j.Logger;
/**
 * Created by Ilya on 23.06.2014.
 */
public class Main {
    public static void main(final String[] args) {
        Logger log = Logger.getLogger(Main.class);
        log.debug("Start code formatter");

        String inputPath =  new String();
        String outputPath = new String();
        String configPath = "format.properties";
        InStream code;
        OutStream formattedCode;
        FormatOptions options;
        CodeFormatter codeFormatter;

        if (args.length == 2){
            inputPath = args[0];
            outputPath = args[1];
        }

        if (inputPath.isEmpty() || outputPath.isEmpty()){
            log.fatal(help());
            return;
        }

        try {
            code = new FileInStream(inputPath);
            formattedCode = new FileOutStream(outputPath);
            options = new FormatOptions(configPath);
            codeFormatter = new CodeFormatter();

            codeFormatter.format(code, formattedCode, options);

            code.close();
            formattedCode.close();
        } catch (NotEnoughBracketsException e) {
            log.fatal(e.getMessage());
            return;
        } catch (StreamException e) {
            log.fatal(e.getMessage());
            return;
        } catch (FormatterException e){
            log.error(e.getMessage());
            return;
        }

        log.info("Formatting complete");
    }

    private static String help(){
        return "Please use flags: formatter INPUT_FILE OUTPUT_FILE";
    }
}
