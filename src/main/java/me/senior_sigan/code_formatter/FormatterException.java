package me.senior_sigan.code_formatter;

/**
 * Created by Ilya on 28.06.2014.
 */
public class FormatterException extends Exception {
    public FormatterException(){
        super();
    }

    public FormatterException(String message){
        super(message);
    }
    public FormatterException(String message, Throwable cause) {
        super(message, cause);
    }

    public FormatterException(Throwable cause) {
        super(cause);
    }
}
