package me.senior_sigan.code_formatter;

import me.senior_sigan.code_formatter.streams.FileInStream;
import me.senior_sigan.code_formatter.streams.StreamException;
import me.senior_sigan.code_formatter.streams.StringInStream;
import me.senior_sigan.code_formatter.streams.StringOutStream;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;

/**
 * Created by Ilya on 29.06.2014.
 */
public class CodeFormatterTest {
    private CodeFormatter formatter;
    private FormatOptions options;

    @Before
    public void setup(){
        formatter = new CodeFormatter();
        options = new FormatOptions();
    }

    /**
     * Test Stream Exception for FileInStream Class
     * @throws StreamException
     * @throws FormatterException
     */
    @Test(expected = StreamException.class)
    public void testFileInStream() throws StreamException, FormatterException {
        FileInStream inStream = new FileInStream("somewhere");
        StringOutStream outStream = new StringOutStream();
        formatter.format(inStream, outStream, options);
    }

    /**
     * Test not enough brackets exception
     */
    @Test(expected = NotEnoughBracketsException.class)
    public void testBrackets() throws FormatterException{
        String badCode = "public class Class { void main(){}";
        try {
            formatter.format(new StringInStream(badCode), new StringOutStream(), options);
        } catch (StreamException e){
            // do nothing
        }
    }

    /**
     * Test Formatter and options: using spaces, indention, crlf-lf
     */
    @Test
    public void testFormattingWithSpaces(){
        String code = "public void someMethod(int a){for(int i=0;i<a;i++){bum(i)}}";
        options.useSpaces();
        options.setIndentSize(2);
        options.useLF();
        String resCode =
                "public void someMethod(int a) {" +
                        "\n  for(int i=0;i<a;i++) {" +
                        "\n    bum(i)" +
                        "\n  }" +
                        "\n}";
        StringOutStream res = new StringOutStream();
        try {
            formatter.format(new StringInStream(code), res, options);
        } catch (FormatterException e){
        }catch (StreamException e){}
        assertEquals(resCode, res.show());
    }

    /**
     * Test Formatter and options: using tabs, indention, crlf-lf
     */
    @Test
    public void testFormattingWithTabs(){
        String code = "public void someMethod(int a){for(int i=0;i<a;i++){bum(i)}}";
        options.useTabs();
        options.setIndentSize(1);
        options.useLF();
        String resCode =
                "public void someMethod(int a) {" +
                        "\n\tfor(int i=0;i<a;i++) {" +
                        "\n\t\tbum(i)" +
                        "\n\t}" +
                        "\n}";
        StringOutStream res = new StringOutStream();
        try {
            formatter.format(new StringInStream(code), res, options);
        } catch (FormatterException e){}
        catch(StreamException e){}
        assertEquals(resCode, res.show());
    }
}
